/**
 * Handle multiple client socket connections on multiple
 * host ports.
 *
 * VanguardiaSur (c) 2015 - ezequiel@vanguardiasur.com.ar
 *
 * Based on code by Silver Moon (m00n.silv3r@gmail.com)
 * http:// www.binarytides.com/multiple-socket-connections-fdset-select-linux/
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>

#define PORT_START	8888
#define PORT_COUNT	5
#define MAX_CLIENTS	50
#define MAX_BACKLOG	10

int main()
{
	int server_socks[PORT_COUNT];
	int sockets[MAX_CLIENTS];
	int fd;
	struct sockaddr_in address;
	char buffer[1025];  // data buffer of 1K
	int i, j;

	fd_set readfds;

	// initialise all sockets[] to 0 so not checked
	for (i = 0; i < MAX_CLIENTS; i++)
		sockets[i] = -1;

	// create a master socket
	for (i = 0; i < PORT_COUNT; i++) {
		if ((server_socks[i] = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
			perror("socket failed");
			exit(EXIT_FAILURE);
		}

		// set master socket to allow multiple connections,
		// this is just a good habit, it will work without this
		//if (setsockopt(server_socks[i], SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 ) {
		//	perror("setsockopt");
		//	exit(EXIT_FAILURE);
		//}

		// type of socket created
		address.sin_family = AF_INET;
		address.sin_addr.s_addr = INADDR_ANY;
		address.sin_port = htons(PORT_START + i);

		// bind the socket to localhost port 8888 + i
		if (bind(server_socks[i], (struct sockaddr *)&address, sizeof(address))<0) {
			perror("bind failed");
			exit(EXIT_FAILURE);
		}

		// specify maximum of pending connections
		// for the server socket
		if (listen(server_socks[i], MAX_BACKLOG) < 0) {
			perror("listen");
			exit(EXIT_FAILURE);
		}
		printf("Listening on port %d\n", PORT_START + i);
	}

	// accept the incoming connection
	printf("Waiting for connections ...\n");

	while (1) {
		int addrlen = sizeof(address);
		int max_sd = -1;
		int activity;

		// clear the socket set
		FD_ZERO(&readfds);

		for (i = 0; i < PORT_COUNT; i++) {
			// add master socket to set
			FD_SET(server_socks[i], &readfds);

			if (server_socks[i] > max_sd)
				max_sd = server_socks[i];
		}

		// add child sockets to set
		for (i = 0; i < MAX_CLIENTS; i++) {
			// if valid socket descriptor then add to read list
			if (sockets[i] >= 0)
				FD_SET(sockets[i], &readfds);

			// highest file descriptor number,
			// needed for the select function
			if (sockets[i] > max_sd)
				max_sd = sockets[i];
		}

		// wait for an activity on *any* of the sockets,
		// timeout is NULL, so wait indefinitely
		activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);
		if ((activity < 0) && (errno!=EINTR)) {
			printf("select error");
		}

		// If something happened on the server socket,
		// then its an incoming connection
		for (i = 0; i < PORT_COUNT; i++) {
			if (FD_ISSET(server_socks[i], &readfds)) {
				int new_socket;

				if ((new_socket = accept(server_socks[i], (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
					perror("accept");
					exit(EXIT_FAILURE);
				}

				// inform user of socket number,
				// used in send and receive commands
				printf("New connection: socket fd=%d, ip=%s, port=%d (%d)\n",
					new_socket, inet_ntoa(address.sin_addr),
					ntohs(address.sin_port), PORT_START + i);
				// add new socket to array of sockets
				for (j = 0; j < MAX_CLIENTS; j++) {
					// if position is empty
					if (sockets[j] == -1) {
						sockets[j] = new_socket;
						printf("Adding to list of sockets as %d\n" , j);
						break;
					}
				}
			}
		}

		// else its some IO operation on some other socket
		for (i = 0; i < MAX_CLIENTS; i++) {
			int valread;

			fd = sockets[i];
			if (fd < 0)
				continue;

			if (FD_ISSET(fd , &readfds)) {
				// Check if it was for closing,
				// and also read the incoming message
				if ((valread = read(fd, buffer, 1024)) == 0) {
					// Somebody disconnected,
					// get his details and print
					getpeername(fd, (struct sockaddr*)&address , (socklen_t*)&addrlen);
					printf("Host disconnected, ip=%s, port=%d\n",
						inet_ntoa(address.sin_addr),
						ntohs(address.sin_port));

					// Close the socket and mark as 0 in
					// list for reuse
					close(fd);
					sockets[i] = -1;
				} else {
					// set the string terminating NULL byte
					// on the end of the data read
					buffer[valread] = '\0';
					if (send(fd, buffer, strlen(buffer),MSG_NOSIGNAL) != (int)strlen(buffer))
						perror("send");
				}
			}
		}
	}

	return 0;
}

