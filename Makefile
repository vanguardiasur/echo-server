.PHONY: all clean re test

NAME=echo-test srv
all: $(NAME)

%: %.c
	$(CC) -lpthread -Wall -Wextra $< -o $@

clean:
	rm -f $(NAME)

re: clean all
