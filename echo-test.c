#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>

#define PORT_START	8888
#define PORT_COUNT	5
#define N_THREADS	50

void die(char *s)
{
	perror(s);
	abort();
}

void *th_main(void *arg)
{
	struct addrinfo *ainf = arg;
	struct sockaddr_in sin;
	int sock;
	char msg[512];
	char res[512];
	int i, l;
	int ret;

	memcpy(&sin, ainf->ai_addr, sizeof sin);
	sin.sin_port = htons(PORT_START + rand() % PORT_COUNT);

	sock = socket(ainf->ai_family, ainf->ai_socktype, ainf->ai_protocol);
	if (sock < 0)
		die("sock");

	ret = connect(sock, (const struct sockaddr*)&sin, sizeof sin);
	if (ret)
		die("connect");

	for (;;) {
		l = rand() % sizeof msg;
		if (l == 0)
			l = 1;

		for (i = 0; i < l; i++)
			msg[i] = rand() % (1 + 'z' - 'a') + 'a';
		msg[l] = 0;

		ret = send(sock, msg, l, 0);
		if (ret != l)
			die("send");

		ret = recv(sock, res, l, MSG_WAITALL);
		if (ret != l)
			die("recv");
		res[l] = 0;

		if (strcmp(msg, res)) {
			fprintf(stderr, "MISMATCH!!!\n");
			fprintf(stderr, "msg = <%s>\n", msg);
			fprintf(stderr, "res = <%s>\n", res);
			abort();
		}

		/* Print a dot when all is good */
		printf(".");
	}
}

int main(int argc, char **argv)
{
	int rc;
	struct addrinfo *ainf;
	struct addrinfo hints;
	pthread_t th[N_THREADS];
	int i;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <host>\n", argv[0]);
		exit(1);
	}

	rc = getaddrinfo(argv[1], NULL, &hints, &ainf);
	if (rc) {
		fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(rc));
		die("getaddrinfo");
	}

	for (i = 0; i < N_THREADS; i++)
		pthread_create(&th[i], NULL, th_main, ainf);

	for (i = 0; i < N_THREADS; i++)
		pthread_join(th[i], NULL);

	return 0;
}
